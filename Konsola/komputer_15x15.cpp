/**
  * \file komputer_15x15.cpp
  * \brief Zawiera funkcje obsługujące inteligentną grę komputera w wersji 15x15
  */

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <Naglowek.h>
#include <ctime>
#include <cstdlib>


using namespace std;

void ruch_komputera_15x15(int **tab, int w, int k, int &ktory_ruch, int ile_wygrywa)
{
    if (ktory_ruch==1)
    {
        pierwszy_ruch(tab);
        return;
    }
    if (ktory_ruch==2)
    {
        postaw_obok(tab,w,k);
        return;
    }
    if (!czy_stawiamy_5ty_krzyzyk(tab,w,k,ile_wygrywa))
    {
        if (!czy_przeciwnik_stawia_5te_kolko(tab,w,k,ile_wygrywa))
        {
            if (!wygrana_w_2_ruchy(tab,w,k))
            {
                if(!czy_stawiamy_4ty_krzyzyk(tab,w,k,ile_wygrywa))
                {
                    if(!przegrana_w_2_ruchy(tab,w,k))
                    {
                        if(!czy_przeciwnik_stawia_4te_kolko(tab,w,k,ile_wygrywa))
                        {
                            if(!sensowny_losowy_15x15(tab))
                            {
                                losuj_15x15(tab);
                            }
                        }

                    }
                }

            }
        }
    }
}
void losuj_15x15(int **tab)
{
    int wiersz=-1,kolumna=-1;
    wiersz= (rand()%15);
    kolumna= (rand()%15);

    while (tab[wiersz][kolumna]!=puste)
    {
        wiersz= (rand()%15);
        kolumna= (rand()%15);
    }
    tab[wiersz][kolumna]=krzyzyk;
    stawiamy_krzyzyk(tablica_krzyzykow,wiersz,kolumna);
}
bool czy_stawiamy_5ty_krzyzyk(int **tab, int w, int k, int ile_wygrywa)
{
    bool wygrana=false;
    //wiersze
    for (int i=0;i<w;i++)
    {
        bool czy_sprawdzamy=false;
        int ile_krzyzykow_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>3){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int j=0;j<(k-4);j++)
            {
                if (tab[i][j]==krzyzyk||tab[i][j+1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i][j+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i][j+p]==kolko){ile_kolek++;}
                          else if (tab[i][j+p]==puste){ile_pustych++; kolumna_pustych=(j+p);}
                    }
                    if (ile_krzyzykow==4&&ile_pustych==1)
                    {
                        tab[i][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        bool czy_sprawdzamy=false;
        int ile_krzyzykow_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>3){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int i=0;i<(w-4);i++)
            {
                if (tab[i][j]==krzyzyk||tab[i+1][j]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+p][j]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+p][j]==kolko){ile_kolek++;}
                          else if (tab[i+p][j]==puste){ile_pustych++; wiersz_pustych=(i+p);}
                    }
                    if (ile_krzyzykow==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,j);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // zwykle przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=0;j<(k-ile_wygrywa+1);j++)
        {
            bool czy_sprawdzamy=false;
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>3){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((l+i)<(w-4))&&((l+j)<(k-4));l++)
            {
                if (tab[i+l][j+l]==krzyzyk||tab[i+l+1][j+l+1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+l+p][j+l+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j+l+p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j+l+p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j+l+p);}
                    }
                    if (ile_krzyzykow==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    // odwrotne przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=(k-1);j>(ile_wygrywa-2);j--)
        {
            bool czy_sprawdzamy=false;
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>3){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((i+l)<(w-4))&&((j-l)>3);l++)
            {
                if (tab[i+l][j-l]==krzyzyk||tab[i+l+1][j-l-1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+l+p][j-l-p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j-l-p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j-l-p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j-l-p);}
                    }
                    if (ile_krzyzykow==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    return wygrana;
}
bool czy_przeciwnik_stawia_5te_kolko(int **tab, int w, int k, int ile_wygrywa)
{
    bool wygrana=false;
    //wiersze
    for (int i=0;i<w;i++)
    {
        bool czy_sprawdzamy=false;
        int ile_kolek_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>3){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int j=0;j<(k-4);j++)
            {
                if (tab[i][j]==kolko||tab[i][j+1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i][j+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i][j+p]==kolko){ile_kolek++;}
                          else if (tab[i][j+p]==puste){ile_pustych++; kolumna_pustych=(j+p);}
                    }
                    if (ile_kolek==4&&ile_pustych==1)
                    {
                        tab[i][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        bool czy_sprawdzamy=false;
        int ile_kolek_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>3){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int i=0;i<(w-4);i++)
            {
                if (tab[i][j]==kolko||tab[i+1][j]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+p][j]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+p][j]==kolko){ile_kolek++;}
                          else if (tab[i+p][j]==puste){ile_pustych++; wiersz_pustych=(i+p);}
                    }
                    if (ile_kolek==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,j);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // zwykle przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=0;j<(k-ile_wygrywa+1);j++)
        {
            bool czy_sprawdzamy=false;
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>3){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((l+i)<(w-4))&&((l+j)<(k-4));l++)
            {
                if (tab[i+l][j+l]==kolko||tab[i+l+1][j+l+1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+l+p][j+l+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j+l+p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j+l+p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j+l+p);}
                    }
                    if (ile_kolek==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    // odwrotne przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=(k-1);j>(ile_wygrywa-2);j--)
        {
            bool czy_sprawdzamy=false;
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>3){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((i+l)<(w-4))&&((j-l)>3);l++)
            {
                if (tab[i+l][j-l]==kolko||tab[i+l+1][j-l-1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<5;p++)
                    {
                        if (tab[i+l+p][j-l-p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j-l-p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j-l-p]==puste)
                        {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j-l-p);}
                    }
                    if (ile_kolek==4&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    return wygrana;
}
void pierwszy_ruch (int **tab)
{
    int wiersz=-1,kolumna=-1;
    wiersz= (5 + rand()%5);
    kolumna= (5 + rand()%5);

    while (tab[wiersz][kolumna]!=puste)
    {
        wiersz= (5 + rand()%5);
        kolumna= (5 + rand()%5);
    }
    tab[wiersz][kolumna]=krzyzyk;
    stawiamy_krzyzyk(tablica_krzyzykow,wiersz,kolumna);
}
bool sensowny_losowy_15x15(int **tab)
{
    bool postawione=false;
    int nowy_wiersz=-1;
    int nowa_kolumna=-1;
    int ile_powtorzen=0;
    while(ile_powtorzen<20)
    {

    int ktory_pionek = rand()%postawiono_krzyzykow;
    int wiersz=-1,kolumna=-1;
    if (tablica_krzyzykow[ktory_pionek].numer>0)
    {
    wiersz= tablica_krzyzykow[ktory_pionek].wiersz;
    kolumna= tablica_krzyzykow[ktory_pionek].kolumna;
    }
    else
    {
        postawione=false;
        return postawione;
    }
    nowy_wiersz=wiersz;
    nowa_kolumna=kolumna;
    nowy_wiersz += -1 + rand()%3;
    nowa_kolumna += -1 + rand()%3;
    while(nowy_wiersz<0||nowa_kolumna<0)
    {
        nowy_wiersz += -1 + rand()%3;
        nowa_kolumna += -1 + rand()%3;
    }
    int ile_prob=0;
    while (tab[nowy_wiersz][nowa_kolumna]!=puste||nowy_wiersz<0||nowa_kolumna<0)
    {
        nowy_wiersz=wiersz;
        nowa_kolumna=kolumna;
        nowy_wiersz += -1 + rand()%3;
        nowa_kolumna += -1 + rand()%3;
        while(nowy_wiersz<0||nowa_kolumna<0)
        {
            nowy_wiersz += -1 + rand()%3;
            nowa_kolumna += -1 + rand()%3;
        }
        ile_prob++;
        if(ile_prob>10)break;
    }
    if (tab[nowy_wiersz][nowa_kolumna]==puste&&nowy_wiersz>=0&&nowa_kolumna>=0)
    {
        tab[nowy_wiersz][nowa_kolumna]=krzyzyk;
        stawiamy_krzyzyk(tablica_krzyzykow,nowy_wiersz,nowa_kolumna);
        postawione=true;

        return postawione;
    }
    ile_powtorzen++;
    }
    return postawione;
}
void postaw_obok(int **tab,int w,int k)
{
    for (int i=0;i<w;i++)
    {
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==kolko)
            {
                int wiersz=i;
                int kolumna=j;
                wiersz += (-1 + rand()%3);
                kolumna += (-1 + rand()%3);
                while (tab[wiersz][kolumna]!=puste||wiersz<0||kolumna<0)
                {
                    wiersz=i;
                    kolumna=j;
                    wiersz += (-1 + rand()%3);
                    kolumna += (-1 + rand()%3);
                }
                if (tab[wiersz][kolumna]==puste)
                {
                tab[wiersz][kolumna]=krzyzyk;
                stawiamy_krzyzyk(tablica_krzyzykow,wiersz,kolumna);
                return;
                }
            }
        }
    }
}
bool wygrana_w_2_ruchy( int **tab, int w, int k)
{
    bool wygrana=false;
    // wiersze
    for (int i=0;i<w;i++)
    {
        int ile_krzyzykow_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>2)
        {
            for (int j=1;j<k-3;j++)
            {
                if (tab[i][j]==krzyzyk&&tab[i][j+1]==krzyzyk&&tab[i][j+2]==krzyzyk&&tab[i][j-1]==puste&&tab[i][j+3]==puste)
                {
                    if (j>1&&tab[i][j-2]==puste)
                    {
                        tab[i][j-1]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,j-1);
                        wygrana=true;
                        return wygrana;
                    }
                    if (j<k-4&&tab[i][j+4]==puste)
                    {
                        tab[i][j+3]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,j+3);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }

    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        int ile_krzyzykow_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>2)
        {
            for (int i=1;i<w-3;i++)
            {
                if (tab[i][j]==krzyzyk&&tab[i+1][j]==krzyzyk&&tab[i+2][j]==krzyzyk&&tab[i-1][j]==puste&&tab[i+3][j]==puste)
                {
                    if (i>1&&tab[i-2][j]==puste)
                    {
                        tab[i-1][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i-1,j);
                        wygrana=true;
                        return wygrana;
                    }
                    if (i<w-4&&tab[i+4][j]==puste)
                    {
                        tab[i+3][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i+3,j);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }

    }
    // zwykle przekatne
    for (int i=1;i<w-3;i++)
    {
        for (int j=1; j<k-3;j++)
        {
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>2)
            {
                if (tab[i][j]==krzyzyk&&tab[i+1][j+1]==krzyzyk&&tab[i+2][j+2]==krzyzyk&&tab[i-1][j-1]==puste&&tab[i+3][j+3]==puste)
                {
                    if (i>1&&j>1&&tab[i-2][j-2]==puste)
                    {
                        tab[i-1][j-1]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i-1,j-1);
                        wygrana=true;
                        return wygrana;
                    }
                    if (i<w-4&&j<k-4&&tab[i+4][j+4]==puste)
                    {
                        tab[i+3][j+3]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i+3,j+3);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }

        }
    }
    // odwrotne przekatne
    for (int i=1;i<w-3;i++)
    {
        for (int j=k-2; j>2;j--)
        {
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>2)
            {
                if (tab[i][j]==krzyzyk&&tab[i+1][j-1]==krzyzyk&&tab[i+2][j-2]==krzyzyk&&tab[i-1][j+1]==puste&&tab[i+3][j-3]==puste)
                {
                    if (i>1&&j<k-2&&tab[i-2][j+2]==puste)
                    {
                        tab[i-1][j+1]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i-1,j+1);
                        wygrana=true;
                        return wygrana;
                    }
                    if (i<w-4&&j>1&&tab[i+4][j-4]==puste)
                    {
                        tab[i+3][j-3]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i+3,j-3);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }

        }
    }
    return wygrana;
}
bool przegrana_w_2_ruchy( int **tab, int w, int k)
{
    bool przegrana=false;
    // wiersze
    for (int i=0;i<w;i++)
    {
        int ile_kolek_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>2)
        {
            for (int j=1;j<k-3;j++)
            {


                    if (tab[i][j]==kolko&&tab[i][j+1]==kolko&&tab[i][j+2]==kolko&&tab[i][j-1]==puste&&tab[i][j+3]==puste)
                    {
                        if (j>1&&tab[i][j-2]==puste)
                        {
                            tab[i][j-1]=krzyzyk;
                            stawiamy_krzyzyk(tablica_krzyzykow,i,j-1);
                            przegrana=true;
                            return przegrana;
                        }
                        if (j<k-4&&tab[i][j+4]==puste)
                        {
                            tab[i][j+3]=krzyzyk;
                            stawiamy_krzyzyk(tablica_krzyzykow,i,j+3);
                            przegrana=true;
                            return przegrana;
                        }
                    }


            }
        }

    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        int ile_kolek_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>2)
        {
            for (int i=1;i<w-3;i++)
            {


                    if (tab[i][j]==kolko&&tab[i+1][j]==kolko&&tab[i+2][j]==kolko&&tab[i-1][j]==puste&&tab[i+3][j]==puste)
                    {
                        if (i>1&&tab[i-2][j]==puste)
                        {
                            tab[i-1][j]=krzyzyk;
                            stawiamy_krzyzyk(tablica_krzyzykow,i-1,j);
                            przegrana=true;
                            return przegrana;
                        }
                        if (i<w-4&&tab[i+4][j]==puste)
                        {
                            tab[i+3][j]=krzyzyk;
                            stawiamy_krzyzyk(tablica_krzyzykow,i+3,j);
                            przegrana=true;
                            return przegrana;
                        }
                    }


            }
        }

    }
    // zwykle przekatne
    for (int i=1;i<w-3;i++)
    {
        for (int j=1; j<k-3;j++)
        {
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>2)
            {
                if (tab[i][j]==kolko&&tab[i+1][j+1]==kolko&&tab[i+2][j+2]==kolko&&tab[i-1][j-1]==puste&&tab[i+3][j+3]==puste)
                {
                    if (i>1&&j>1&&tab[i-2][j-2]==puste)
                    {
                        tab[i-1][j-1]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i-1,j-1);
                        przegrana=true;
                        return przegrana;
                    }
                    if (i<w-4&&j<k-4&&tab[i+4][j+4]==puste)
                    {
                        tab[i+3][j+3]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i+3,j+3);
                        przegrana=true;
                        return przegrana;
                    }
                }
            }

        }
    }
    // odwrotne przekatne
    for (int i=1;i<w-3;i++)
    {
        for (int j=k-2; j>2;j--)
        {
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>2)
            {
                if (tab[i][j]==kolko&&tab[i+1][j-1]==kolko&&tab[i+2][j-2]==kolko&&tab[i-1][j+1]==puste&&tab[i+3][j-3]==puste)
                {
                    if (i>1&&j<k-2&&tab[i-2][j+2]==puste)
                    {
                        tab[i-1][j+1]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i-1,j+1);
                        przegrana=true;
                        return przegrana;
                    }
                    if (i<w-4&&j>1&&tab[i+4][j-4]==puste)
                    {
                        tab[i+3][j-3]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i+3,j-3);
                        przegrana=true;
                        return przegrana;
                    }
                }
            }

        }
    }
    return przegrana;
}
bool czy_stawiamy_4ty_krzyzyk(int **tab, int w, int k, int ile_wygrywa)
{
    bool wygrana=false;
    //wiersze
    for (int i=0;i<w;i++)
    {
        bool czy_sprawdzamy=false;
        int ile_krzyzykow_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>2){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int j=0;j<(k-3);j++)
            {
                if (tab[i][j]==krzyzyk||tab[i][j+1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i][j+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i][j+p]==kolko){ile_kolek++;}
                          else if (tab[i][j+p]==puste){ile_pustych++; kolumna_pustych=(j+p);}
                    }
                    if (ile_krzyzykow==3&&ile_pustych==1)
                    {
                        tab[i][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        bool czy_sprawdzamy=false;
        int ile_krzyzykow_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==krzyzyk)
            {
                ile_krzyzykow_lacznie++;
            }
        }
        if (ile_krzyzykow_lacznie>2){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int i=0;i<(w-3);i++)
            {
                if (tab[i][j]==krzyzyk||tab[i+1][j]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+p][j]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+p][j]==kolko){ile_kolek++;}
                          else if (tab[i+p][j]==puste){ile_pustych++; wiersz_pustych=(i+p);}
                    }
                    if (ile_krzyzykow==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,j);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // zwykle przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=0;j<(k-ile_wygrywa+1);j++)
        {
            bool czy_sprawdzamy=false;
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>2){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((l+i)<(w-3))&&((l+j)<(k-3));l++)
            {
                if (tab[i+l][j+l]==krzyzyk||tab[i+l+1][j+l+1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+l+p][j+l+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j+l+p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j+l+p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j+l+p);}
                    }
                    if (ile_krzyzykow==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    // odwrotne przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=(k-1);j>(ile_wygrywa-3);j--)
        {
            bool czy_sprawdzamy=false;
            int  ile_krzyzykow_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==krzyzyk)
                {
                    ile_krzyzykow_lacznie++;
                }
            }
            if (ile_krzyzykow_lacznie>3){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((i+l)<(w-3))&&((j-l)>2);l++)
            {
                if (tab[i+l][j-l]==krzyzyk||tab[i+l+1][j-l-1]==krzyzyk)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+l+p][j-l-p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j-l-p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j-l-p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j-l-p);}
                    }
                    if (ile_krzyzykow==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    return wygrana;
}
bool czy_przeciwnik_stawia_4te_kolko(int **tab, int w, int k, int ile_wygrywa)
{
    bool wygrana=false;
    //wiersze
    for (int i=0;i<w;i++)
    {
        bool czy_sprawdzamy=false;
        int ile_kolek_lacznie=0;
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>2){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int j=0;j<(k-3);j++)
            {
                if (tab[i][j]==kolko||tab[i][j+1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i][j+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i][j+p]==kolko){ile_kolek++;}
                          else if (tab[i][j+p]==puste){ile_pustych++; kolumna_pustych=(j+p);}
                    }
                    if (ile_kolek==3&&ile_pustych==1)
                    {
                        tab[i][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,i,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // kolumny
    for (int j=0;j<k;j++)
    {
        bool czy_sprawdzamy=false;
        int ile_kolek_lacznie=0;
        for (int i=0;i<w;i++)
        {
            if (tab[i][j]==kolko)
            {
                ile_kolek_lacznie++;
            }
        }
        if (ile_kolek_lacznie>2){czy_sprawdzamy=true;}
        else {czy_sprawdzamy=false;}

        if (czy_sprawdzamy)
        {
            for (int i=0;i<(w-3);i++)
            {
                if (tab[i][j]==kolko||tab[i+1][j]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+p][j]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+p][j]==kolko){ile_kolek++;}
                          else if (tab[i+p][j]==puste){ile_pustych++; wiersz_pustych=(i+p);}
                    }
                    if (ile_kolek==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][j]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,j);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
        }
    }
    // zwykle przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=0;j<(k-ile_wygrywa+1);j++)
        {
            bool czy_sprawdzamy=false;
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((l+j)<k);l++)
            {
                if(tab[i+l][j+l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>2){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((l+i)<(w-3))&&((l+j)<(k-3));l++)
            {
                if (tab[i+l][j+l]==kolko||tab[i+l+1][j+l+1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+l+p][j+l+p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j+l+p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j+l+p]==puste)
                            {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j+l+p);}
                    }
                    if (ile_kolek==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    // odwrotne przekatne
    for (int i=0;i<(w-ile_wygrywa+1);i++)
    {
        for (int j=(k-1);j>(ile_wygrywa-3);j--)
        {
            bool czy_sprawdzamy=false;
            int  ile_kolek_lacznie=0;
            for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
            {
                if(tab[i+l][j-l]==kolko)
                {
                    ile_kolek_lacznie++;
                }
            }
            if (ile_kolek_lacznie>2){czy_sprawdzamy=true;}
            else {czy_sprawdzamy=false;}
            if (czy_sprawdzamy)
            {
            for (int l=0;((i+l)<(w-3))&&((j-l)>2);l++)
            {
                if (tab[i+l][j-l]==kolko||tab[i+l+1][j-l-1]==kolko)
                {
                    int ile_krzyzykow=0, ile_kolek=0, ile_pustych=0, wiersz_pustych=-1, kolumna_pustych=-1;
                    for (int p=0;p<4;p++)
                    {
                        if (tab[i+l+p][j-l-p]==krzyzyk){ile_krzyzykow++;}
                         else if (tab[i+l+p][j-l-p]==kolko){ile_kolek++;}
                          else if (tab[i+l+p][j-l-p]==puste)
                        {ile_pustych++; wiersz_pustych=(i+l+p);kolumna_pustych=(j-l-p);}
                    }
                    if (ile_kolek==3&&ile_pustych==1)
                    {
                        tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
                        stawiamy_krzyzyk(tablica_krzyzykow,wiersz_pustych,kolumna_pustych);
                        wygrana=true;
                        return wygrana;
                    }
                }
            }
            }
        }
    }
    return wygrana;
}
