/**
  * \file menu.cpp
  *\brief Moduł obsługujący wyświetlenie menu i przekazanie do modułu głównego informacji o wybranym rodzaju gry
  */

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <Naglowek.h>

using namespace std;


int menu(int &w, int &k, int &ile_wygrywa, int rodzaj_gry)
{
    cout<< "KOLKO I KRZYZYK"<<endl<<endl;
    cout<< "***MENU***"<<endl<<endl;
    cout<< "Wybierz opcje:"<<endl;
    cout<< "1   gra 3x3 z partnerem"<<endl;
    cout<< "2   gra 3x3 z komputerem"<<endl;
    cout<< "3   gra 15x15 z partnerem"<<endl;
    cout<< "4   gra 15x15 z komputerem"<<endl;
    cout<< "5   koniec gry"<<endl<<endl;
    cout<< "Wybierz i wpisz numer, aby uruchomic gre."<<endl;
    bool warunek=true;
    warunek=true;
    while(warunek)
    {
    rodzaj_gry = pobierz_liczbe();
    if ((rodzaj_gry>0)&&(rodzaj_gry)<6)
    {
        warunek=false;
    }
    else
    {
        cout<< "Nieprawidlowy wybor gry"<<endl;
    }
    }
    if ((rodzaj_gry==1)||(rodzaj_gry==2))
    {
        w=3;
        k=3;
        ile_wygrywa=3;
    }
    else if ((rodzaj_gry==3)||(rodzaj_gry==4))
    {
        w=15;
        k=15;
        ile_wygrywa=5;
    }
    system("cls");
    return rodzaj_gry;

}
