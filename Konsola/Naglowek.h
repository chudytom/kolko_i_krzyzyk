/**
  * \file Naglowek.h
  * \brief Plik nagłówkowy zawierający deklaracje wszystkich użytych funkcji i struktur
  */

#ifndef NAGLOWEK_H
#define NAGLOWEK_H
#define puste 0
#define kolko 1
#define krzyzyk 2

/**
 * @brief The Spole struct
 * Przy pomocy tej struktury stworzone są 2 tablice zawierające wszystkie dotychczasowe postawione kółka i krzyżyki
 */
struct Spole
{
    int numer;
    int wiersz;
    int kolumna;
};

/**
 * @brief tworz_tablice Funkcja tworząca tablicę, w której umieszczane są kółka i krzyżyki
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @return      Zwracamy adres utworzonej tablicy
 */
int **tworz_tablice(int w,int k);

/**
 * @brief wypelnij_zerami    Wypełniamy na samym początku tablicę zerami (pustymi polami)
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 */
void wypelnij_zerami(int **tab, int w, int k);

/**
 * @brief drukuj    Drukowanie planszy do gry
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param rodzaj_gry    Rodzaj gry wybranej przez użytkownika w menu
 */
void drukuj (int **tab, int w, int k, int rodzaj_gry);

/**
 * @brief graj  Funkcja kierująca rozgrywką, w niej zawarte są pozostałe funkcje dotyczące gry
 * @param rodzaj_gry    Rodzaj gry wybranej przez użytkownika w menu
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @param numer_gry     Która gra z kolei
 * @return  Informacja o wyborze użytkownika czy chce kontynuować tę samą grę czy wrócić do menu
 */
bool graj(int rodzaj_gry, int **tab, int w, int k, int ile_wygrywa, int numer_gry);

/**
 * @brief sprawdz_wygrana   Funkcja sprawdza czy któryś z graczy wygrał
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @return      Informacja czy jest już wygrana czy nie i kto jest zwycięzcą
 */
int sprawdz_wygrana(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief wykonaj_ruch  Funkcja przekazująca informację jaki ruch został wykonany
 * W zależności od rodzaju gry, wybor która funckja jest za to odpowiedzialna
 * @param rodzaj_gry    Rodzaj gry wybranej przez użytkownika w menu
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @param numer_gry     Która gra z kolei
 * @param ktory_ruch    Który ruch z kolei w danej grze
 * @return      Informacja czy kontynuujemy grę
 */
bool wykonaj_ruch(int rodzaj_gry, int **tab, int w, int k, int ile_wygrywa, int numer_gry, int &ktory_ruch);

/**
 * @brief menu  Wyświetlenie menu i przekazanie wyboru użytkownika do modułu głównego
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @param rodzaj_gry    Rodzaj gry wybranej przez użytkownika w menu
 * @return      Przekazanie rodzaju wybranej gry
 */
int menu(int &w, int &k, int &ile_wygrywa, int rodzaj_gry);

/**
 * @brief pobierz_liczbe    Pobranie liczby, bez żadnych znaków specjalnych bądź liter
 * @return      Zwracamy wpisaną liczbę
 */
int pobierz_liczbe();

/**
 * @brief ruch_gracza       Pobranie danych o ruchu jaki wykonuje gracz
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param numer_gry     Która gra z kolei
 * @param ktory_ruch    Który ruch z kolei w danej grze
 * @param rodzaj_gry    Rodzaj gry wybranej przez użytkownika w menu
 */
void ruch_gracza(int **tab, int w, int k, int numer_gry, int &ktory_ruch, int rodzaj_gry);

/**
 * @brief ruch_komputera_3x3    Wykonanie ruchu przez komputer w wersji 3x3
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ktory_ruch    Który ruch z kolei w danej grze
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 */
void ruch_komputera_3x3(int **tab, int w, int k, int &ktory_ruch, int ile_wygrywa);

/**
 * @brief czy_mozna_wygrac  Komputer sprawdza czy może postawić 3-ci krzyżyk
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @return      Informacja czy ten 3-ci krzyżyk został postawiony
 */
bool czy_mozna_wygrac(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief czy_mozna_wygrac  Komputer sprawdza czy przeciwnik może postawić 3-cie kółko
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @return      Informacja czy to 3-cie kółko zostało przez gracza postawione
 */
bool czy_mozna_przegrac(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief losuj_3x3     Komputer losuje pole na jakim postawi pionek
 * @param tab       Adres tablicy na której operujemy
 */
void losuj_3x3(int **tab);

/**
 * @brief ruch_komputera_15x15      Wykonanie ruchu przez komputer w wersji 15x15
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ktory_ruch    Który ruch z kolei w danej grze
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygrywa
 */
void ruch_komputera_15x15(int **tab, int w, int k, int &ktory_ruch, int ile_wygrywa);

/**
 * @brief losuj_15x15   Komputer losuje pole na jakim postawi pionek
 * @param tab       Adres tablicy na której operujemy
 */
void losuj_15x15(int **tab);

/**
 * @brief czy_stawiamy_5ty_krzyzyk      Komputer sprawdza czy może postawić 5-ty krzyżyk - wygrywający; Stawia
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa       Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @return      Informacja czy komputer postawił ten 5-ty krzyżyk
 */
bool czy_stawiamy_5ty_krzyzyk(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief czy_przeciwnik_stawia_5te_kolko       Komputer sprawdza czy przeciwnik może postawić 5-te kółko; Broni się
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa       Liczba kółek bądź krzyżyków pod rząd która wygrywa
 * @return      Informacja czy postawiliśmy blokujące kółko
 */
bool czy_przeciwnik_stawia_5te_kolko(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief tworz_tablice_pol     Stworzenie tablicy struktur Spole
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @return      Zwracamy adres tablicy
 */
Spole *tworz_tablice_pol(int w, int k);

/**
 * @brief pierwszy_ruch     Pierwszy ruch komputera w wersji 15x15
 * @param tab       Adres tablicy na której operujemy
 */
void pierwszy_ruch (int **tab);

/**
 * @brief stawiamy_kolko        Przekazanie to tablicy postawionych kołek, gdzie dane kółko zostało postawione
 * @param tab       Adres tablicy na której operujemy
 * @param wiersz        Wiersz, gdzie postawiono kółko
 * @param kolumna       Kolumna, gdzie postawiono kółko
 */
void stawiamy_kolko(Spole *tab,int wiersz,int kolumna);

/**
 * @brief stawiamy_krzyzyk      Przekazanie do tablicy postawionych krzyżyków, gdzie dany krzyżyk został  postawiony
 * @param tab       Adres tablicy na której operujemy
 * @param wiersz        Wiersz, gdzie postawiono krzyżyk
 * @param kolumna       Kolumna, gdzie postawiono krzyżyk
 */
void stawiamy_krzyzyk(Spole *tab,int wiersz,int kolumna);

/**
 * @brief sensowny_losowy_15x15     Komputer wykonuje logiczny ruch
 * Komputer losuje jedną z możliwości ustawienia swojego krzyżyka przyległego do innego swojego krzyżyka
 * @param tab   Adres tablicy na której operujemy
 * @return      Informacja czy taki ruch został wykonany
 */
bool sensowny_losowy_15x15(int **tab);

/**
 * @brief postaw_obok       Pierwszy ruch komputera, gdy przeciwnik zaczyna wersja 15x15
 * Stawiamy krzyżyk w losowym miejscu na około kółka gracza
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 */
void postaw_obok(int **tab,int w,int k);

/**
 * @brief wygrana_w_2_ruchy     Komputer sprawdza czy może wygrać w 2 ruchy
 * Komputer sprawdza czy ma 3 krzyżyki w jednej linii i odpowiednią ilość wolnych pól obok by wygrać
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @return      Informacja czy dostawiono w ten sposób 4-ty krzyżyk
 */
bool wygrana_w_2_ruchy( int **tab, int w, int k);

/**
 * @brief przegrana_w_2_ruchy   Komputer sprawdza czy może przegrać w 2 ruchy
 *Komputer sprawdza czy przeciwnik ma 3 kółka w jednej linii i odpowiednią ilość wolnych pól obok by wygrać
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @return      Informacja zaistniała taka sytuacja
 */
bool przegrana_w_2_ruchy( int **tab, int w, int k);

/**
 * @brief czy_stawiamy_4ty_krzyzyk  Komputer sprawdza czy moze wygrać w 2 ruchy
 * Komputer sprawdza czy są na 4 polach 3 krzyżyki i przerwa między nimi i wolne pola obok czwórki
 * Stawia krzyżyk w środek
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyzyków pod rząd która wygrywa
 * @return      Informacja czy ten 4-ty krzyżyk został postawiony
 */
bool czy_stawiamy_4ty_krzyzyk(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief czy_przeciwnik_stawia_4te_kolko Komputer sprawdza czy przeciwnik może wygrać w 2 ruchy
 * Komputer sprawdza czy są na 4 polach 3 kółka i przerwa między nimi i wolne pola obok
 * Stawia krzyżyk w środek
 * @param tab   Adres tablicy na której operujemy
 * @param w     Rozmiar planszy (ilość wierszy)
 * @param k     Rozmiar planszy (ilość kolumn)
 * @param ile_wygrywa   Liczba kółek bądź krzyżyków pod rząd która wygyrywa
 * @return      Informacja czy zaistniała taka sytuacja
 */
bool czy_przeciwnik_stawia_4te_kolko(int **tab, int w, int k, int ile_wygrywa);

/**
 * @brief wygrane_kolka      Deklaracja zmiennej mówiącej ile razy wygrał gracz kółko
 */
extern int wygrane_kolka;

/**
 * @brief wygrane_krzyzyka      Deklaracja zmiennej mówiącej ile razy wygrał gracz krzyżyk
 */
extern int wygrane_krzyzyka;

/**
 * @brief postawiono_kolek      Deklaracja zmiennej mówiącej ile kółek postawiono na planszy w danej grze
 */
extern int postawiono_kolek;

/**
 * @brief postawiono_krzyzykow      Deklaracja zmiennej mówiącej ile krzyżyków postawiono na planszy w danej grze
 */
extern int postawiono_krzyzykow;

/**
 * @brief tablica_kolek     Deklaracja wskaźnika to tablicy zawierającej wszystkie postawione kółka w danej grze
 */
extern Spole *tablica_kolek;

/**
 * @brief tablica_krzyzykow     Deklaracja wskaźnika to tablicy zawierającej wszystkie postawione krzyżyki w danej grze
 */
extern Spole *tablica_krzyzykow;

#endif // NAGLOWEK_H
