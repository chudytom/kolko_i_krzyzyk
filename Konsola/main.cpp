/**
  * \mainpage
  * \par Kółko i krzyżyk
  * Program do gry w kółko i krzyżyk z przeciwnikiem i komputerem w wersji 3x3 i 15x15
  * \author Tomasz Chudzik
  * \date 20.05.2014
  * \version 3.0
  * \par Kontakt
  * \a 94tom94@gmail.com
  */

/**
  * \file main.cpp
  * \brief Główny moduł programu
  */


#include <iostream>
#include <windows.h>
#include <conio.h>
#include <Naglowek.h>
#include <ctime>
#include <cstdlib>

/**
 * @brief wygrane_kolka
 * Liczba zwycięstw grającego kółkami
 */
int wygrane_kolka=0;
/**
 * @brief wygrane_krzyzyka
 * Liczba zwycięstw grającego krzyżykami
*/
int wygrane_krzyzyka=0;

using namespace std;



int main()
{
    srand(time(NULL));
    // gracz - kolko    komputer - krzyzyk
    int w=0,k=0,ile_wygrywa=0,rodzaj_gry=0;
    while (rodzaj_gry!=5)
    {
    wygrane_kolka=0;
    wygrane_krzyzyka=0;
    rodzaj_gry=menu(w,k,ile_wygrywa,rodzaj_gry);
    bool czy_gramy=true;

    if (rodzaj_gry==5)
    {
        czy_gramy=false;
        cout<< "Koniec gry"<<endl<<endl<<endl;
    }
    int numer_gry=0;
    while(czy_gramy)
    {
    int **plansza=tworz_tablice(w,k);
    numer_gry++;
    wypelnij_zerami(plansza,w,k);    
    czy_gramy = graj(rodzaj_gry, plansza,w,k,ile_wygrywa,numer_gry);
    }
    system ("cls");
    }
}
