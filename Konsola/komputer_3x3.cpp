/**
  * \file komputer_3x3.cpp
  * \brief Zawiera funkcje obsługujące inteligentną grę komputera w wersji 3x3
  */

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <Naglowek.h>
#include <ctime>
#include <cstdlib>


using namespace std;

void ruch_komputera_3x3(int **tab, int w, int k, int &ktory_ruch, int ile_wygrywa)
{
    if (czy_mozna_wygrac(tab,w,k,ile_wygrywa))
    {
        //cout<< "Jest wygrana"<<endl<<endl;
        //Sleep(2000);
    }
    else if (czy_mozna_przegrac(tab,w,k,ile_wygrywa))
        {
            //cout<< "Grozi przegrana"<<endl<<endl;
            //Sleep(2000);
        }
    // ****************** zaczyna gracz ******************************************************************************
    else if (ktory_ruch==2) //zaczyna gracz 2gi ruch
    {
        if (tab[1][1]==kolko) // stawia na srodku
        {
            int ktory_rog=0;
            ktory_rog=1 + rand()%4;
            if (ktory_rog==1) {tab[0][0]=krzyzyk;}
            else if (ktory_rog==2) {tab[0][2]=krzyzyk;}
            else if (ktory_rog==3) {tab[2][0]=krzyzyk;}
            else if (ktory_rog==4) {tab[2][2]=krzyzyk;}
        }
        else // nie stawia na srodku
        {
            tab[1][1]=krzyzyk;
            stawiamy_krzyzyk(tablica_krzyzykow,1,1);
            return;
        }
    }
    else if ((ktory_ruch==4)&&(tab[1][1]==kolko)) //zaczyna gracz 4ty ruch
    {
        if ((tab[0][0]==kolko)&&(tab[2][2]==krzyzyk)) {tab[0][2]=krzyzyk;}
        else if ((tab[2][2]==kolko)&&(tab[0][0]==krzyzyk)) {tab[2][0]=krzyzyk;  return;}
        else if ((tab[0][2]==kolko)&&(tab[2][0]==krzyzyk)) {tab[2][2]=krzyzyk; return;}
        else if ((tab[2][0]==kolko)&&(tab[0][2]==krzyzyk)) {tab[0][0]=krzyzyk; return;}
    }
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(((tab[0][0]==kolko)&&(tab[2][2]==kolko))||((tab[0][2]==kolko)&&(tab[2][0]==kolko))))
    {
        int ktory_rog=0;
        ktory_rog= 1 + rand()%4;
        if (ktory_rog==1) { tab[0][1]=krzyzyk; return;}
        if (ktory_rog==2) { tab[1][0]=krzyzyk; return;}
        if (ktory_rog==3) { tab[2][1]=krzyzyk; return;}
        if (ktory_rog==4) { tab[1][2]=krzyzyk; return;}
    }
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[2][0]==kolko)){ tab[0][2]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[0][2]==kolko)){ tab[2][0]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[0][0]==kolko)){ tab[2][2]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[2][2]==kolko)){ tab[0][0]=krzyzyk; return;}
    // 2 na srodkach bokow jak litera 'l'
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[1][0]==kolko)&&(tab[0][1]==kolko)){ tab[0][0]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[1][0]==kolko)&&(tab[2][1]==kolko)){ tab[2][0]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[0][1]==kolko)&&(tab[1][2]==kolko)){ tab[0][2]=krzyzyk; return;}
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[1][2]==kolko)&&(tab[2][1]==kolko)){ tab[2][2]=krzyzyk; return;}
    // 2 na srodkach bokow po obu stronach srodka
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(((tab[1][0]==kolko)&&(tab[1][2]==kolko))||((tab[0][1]==kolko)&&(tab[2][1]==kolko))))
    {
        int ktory_rog=0;
        ktory_rog = 1 + rand()%4;
        if (ktory_rog==1) { tab[0][0]=krzyzyk; return;}
        if (ktory_rog==2) { tab[2][0]=krzyzyk; return;}
        if (ktory_rog==3) { tab[0][2]=krzyzyk; return;}
        if (ktory_rog==4) { tab[2][2]=krzyzyk; return;}
    }
    else if ((ktory_ruch==4)&&(tab[1][1]==krzyzyk)&&(tab[0][1]==kolko)&&(tab[2][1]==kolko)){ tab[2][0]=krzyzyk; return;}

    // ****************** zaczyna komputer ******************************************************************************
    else if (ktory_ruch==1)
    {
        int jaki_poczatek=0;
        jaki_poczatek = 1 + rand()%8;
        if ((jaki_poczatek>=1)&&(jaki_poczatek<=4)){ tab[1][1]=krzyzyk; return;}
        else if (jaki_poczatek==5){tab[0][0]=krzyzyk; return;}
        else if (jaki_poczatek==6){tab[0][2]=krzyzyk; return;}
        else if (jaki_poczatek==7){tab[2][0]=krzyzyk; return;}
        else if (jaki_poczatek==8){tab[2][2]=krzyzyk; return;}
    }
    else if(ktory_ruch==3&&tab[1][1]==krzyzyk)  // trzeci ruch gdy pierwszy w srodku

    {
        //gracz nie stawia w rogu
        if ((tab[1][2]==kolko)||(tab[2][1]==kolko)){ tab[0][0]=krzyzyk; return; }
        else if ((tab[1][0]==kolko)||(tab[0][1]==kolko)){ tab[2][2]=krzyzyk; return; }
        // gracz stawia w rogu
        int jaki_trzeci_ruch=0;
        jaki_trzeci_ruch= 1 + rand()%2;
        if ((tab[0][0]==kolko)&&(jaki_trzeci_ruch==1)){ tab[2][2]=krzyzyk; return; }
        if ((tab[0][2]==kolko)&&(jaki_trzeci_ruch==1)){ tab[2][0]=krzyzyk; return; }
        if ((tab[2][0]==kolko)&&(jaki_trzeci_ruch==1)){ tab[0][2]=krzyzyk; return; }
        if ((tab[2][2]==kolko)&&(jaki_trzeci_ruch==1)){ tab[0][0]=krzyzyk; return; }
        else{losuj_3x3(tab); return;}
    }
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[2][2]==krzyzyk && tab[0][0]==kolko && tab[1][2]==kolko)
    { tab[2][0]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[2][2]==krzyzyk && tab[0][0]==kolko && tab[2][1]==kolko)
    { tab[0][2]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[2][0]==krzyzyk && tab[0][2]==kolko && tab[1][0]==kolko)
    { tab[2][2]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[2][0]==krzyzyk && tab[0][2]==kolko && tab[2][1]==kolko)
    { tab[0][0]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[0][0]==krzyzyk && tab[2][2]==kolko && tab[0][1]==kolko)
    { tab[2][0]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[0][0]==krzyzyk && tab[2][2]==kolko && tab[1][0]==kolko)
    { tab[0][2]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[0][2]==krzyzyk && tab[2][0]==kolko && tab[1][2]==kolko)
    { tab[0][0]=krzyzyk; return;}
    else if(ktory_ruch==5 && tab[1][1]==krzyzyk && tab[0][2]==krzyzyk && tab[2][0]==kolko && tab[0][1]==kolko)
    { tab[2][2]=krzyzyk; return;}

    else if (ktory_ruch==3&&tab[1][1]==kolko)
    {
        if (tab[0][0]==krzyzyk){tab[2][2]=krzyzyk; return;}
        if (tab[2][2]==krzyzyk){tab[0][0]=krzyzyk; return;}
        if (tab[0][2]==krzyzyk){tab[2][0]=krzyzyk; return;}
        if (tab[2][0]==krzyzyk){tab[0][2]=krzyzyk; return;}
    }
    else if (ktory_ruch==3&&(tab[0][1]==kolko||tab[1][0]==kolko||tab[1][2]==kolko||tab[2][1]==kolko))
    {tab[1][1]=krzyzyk; return;}

    else if (tab[0][0]==kolko||tab[0][2]==kolko||tab[2][2]==kolko||tab[2][0]==kolko)
    {
        if (ktory_ruch==3)
        {
            if (tab[2][2]==krzyzyk&&tab[0][0]==puste){ tab[0][0]=krzyzyk;return;}
            if (tab[0][0]==krzyzyk&&tab[2][2]==puste){ tab[2][2]=krzyzyk;return;}
            if (tab[2][0]==krzyzyk&&tab[0][2]==puste){ tab[0][2]=krzyzyk;return;}
            if (tab[0][2]==krzyzyk&&tab[2][0]==puste){ tab[2][0]=krzyzyk;return;}
            else if(tab[0][0]==krzyzyk||tab[0][2]==krzyzyk||tab[2][2]==krzyzyk||tab[2][0]==krzyzyk)
            {
                if (tab[0][0]==puste){tab[0][0]=krzyzyk;return;}
                if (tab[0][2]==puste){tab[0][2]=krzyzyk;return;}
                if (tab[2][0]==puste){tab[2][0]=krzyzyk;return;}
                if (tab[2][2]==puste){tab[2][2]=krzyzyk;return;}
            }
        }
        else
        {
        int krzyzyki_w_rogu=0,kolka_w_rogu=0;
        if (tab[0][0]==krzyzyk){krzyzyki_w_rogu++;}
        if (tab[0][0]==kolko){kolka_w_rogu++;}
        if (tab[2][2]==krzyzyk){krzyzyki_w_rogu++;}
        if (tab[2][2]==kolko){kolka_w_rogu++;}
        if (tab[0][2]==krzyzyk){krzyzyki_w_rogu++;}
        if (tab[0][2]==kolko){kolka_w_rogu++;}
        if (tab[2][0]==krzyzyk){krzyzyki_w_rogu++;}
        if (tab[2][0]==kolko){kolka_w_rogu++;}
        if (krzyzyki_w_rogu==2&&kolka_w_rogu==1&&ktory_ruch==5)
        {
            if (tab[0][0]==puste){tab[0][0]=krzyzyk;return;}
            if (tab[0][2]==puste){tab[0][2]=krzyzyk;return;}
            if (tab[2][0]==puste){tab[2][0]=krzyzyk;return;}
            if (tab[2][2]==puste){tab[2][2]=krzyzyk;return;}
        }
        else
        {
            losuj_3x3(tab);
        }
        }

    }

    else
    {
        losuj_3x3(tab);
    }
}
bool czy_mozna_wygrac(int **tab, int w, int k, int ile_wygrywa)
{
   bool wygrana=false;
   //wiersze
   for (int i=0;i<w;i++)
   {
       int ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
       for (int j=0;j<k;j++)
       {
           if (tab[i][j]==puste)
           {
               wiersz_pustych=i;
               kolumna_pustych=j;
           }
           else if(tab[i][j]==kolko)
           {
               ile_kolek++;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
           }

       }
       if ((ile_krzyzykow==2)&&(ile_kolek==0))
       {
           tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
           wygrana=true;
           return wygrana;
       }
   }
   // kolumny
   for (int j=0;j<k;j++)
   {
       int ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
       for (int i=0;i<w;i++)
       {
           if (tab[i][j]==puste)
           {
               wiersz_pustych=i;
               kolumna_pustych=j;
           }
           else if(tab[i][j]==kolko)
           {
              ile_kolek++;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
           }
       }
       if ((ile_krzyzykow==2)&&(ile_kolek==0))
       {
           tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
           wygrana=true;
           return wygrana;
       }
   }
   // zwykle przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=0;j<(k-ile_wygrywa+1);j++)
       {
           int  ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
           for (int l=0;((l+i)<w)&&((l+j)<k);l++)
           {
               if (tab[i+l][j+l]==puste)
               {
                   wiersz_pustych=i+l;
                   kolumna_pustych=j+l;
               }
               else if(tab[i+l][j+l]==kolko)
               {
                   ile_kolek++;
               }
               else if(tab[i+l][j+l]==krzyzyk)
               {
                   ile_krzyzykow++;
               }
           }
           if ((ile_krzyzykow==2)&&(ile_kolek==0))
           {
               tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
               wygrana=true;
               return wygrana;
           }
       }
   }
   // odwrotne przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=(k-1);j>(ile_wygrywa-2);j--)
       {
           int  ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
           for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
           {
               if (tab[i+l][j-l]==puste)
               {
                   wiersz_pustych=i+l;
                   kolumna_pustych=j-l;
               }
               else if(tab[i+l][j-l]==kolko)
               {
                  ile_kolek++;
               }
               else if(tab[i+l][j-l]==krzyzyk)
               {
                    ile_krzyzykow++;
               }
           }
           if ((ile_krzyzykow==2)&&(ile_kolek==0))
           {
               tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
               wygrana=true;
               return wygrana;
           }
       }
   }
   return wygrana;
}
bool czy_mozna_przegrac(int **tab, int w, int k, int ile_wygrywa)
{
   bool przegrana=false;
   //wiersze
   for (int i=0;i<w;i++)
   {
       int ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
       for (int j=0;j<k;j++)
       {
           if (tab[i][j]==puste)
           {
               wiersz_pustych=i;
               kolumna_pustych=j;
           }
           else if(tab[i][j]==kolko)
           {
               ile_kolek++;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
           }

       }
       if ((ile_krzyzykow==0)&&(ile_kolek==2))
       {
           tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
           przegrana=true;
           return przegrana;
       }
   }
   // kolumny
   for (int j=0;j<k;j++)
   {
       int ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
       for (int i=0;i<w;i++)
       {
           if (tab[i][j]==puste)
           {
               wiersz_pustych=i;
               kolumna_pustych=j;
           }
           else if(tab[i][j]==kolko)
           {
              ile_kolek++;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
           }
       }
       if ((ile_krzyzykow==0)&&(ile_kolek==2))
       {
           tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
           przegrana=true;
           return przegrana;
       }
   }
   // zwykle przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=0;j<(k-ile_wygrywa+1);j++)
       {
           int  ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
           for (int l=0;((l+i)<w)&&((l+j)<k);l++)
           {
               if (tab[i+l][j+l]==puste)
               {
                   wiersz_pustych=i+l;
                   kolumna_pustych=j+l;
               }
               else if(tab[i+l][j+l]==kolko)
               {
                   ile_kolek++;
               }
               else if(tab[i+l][j+l]==krzyzyk)
               {
                   ile_krzyzykow++;
               }
           }
           if ((ile_krzyzykow==0)&&(ile_kolek==2))
           {
               tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
               przegrana=true;
               return przegrana;
           }
       }
   }
   // odwrotne przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=(k-1);j>(ile_wygrywa-2);j--)
       {
           int  ile_krzyzykow=0, ile_kolek=0, wiersz_pustych=-1, kolumna_pustych=-1;
           for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
           {
               if (tab[i+l][j-l]==puste)
               {
                   wiersz_pustych=i+l;
                   kolumna_pustych=j-l;
               }
               else if(tab[i+l][j-l]==kolko)
               {
                  ile_kolek++;
               }
               else if(tab[i+l][j-l]==krzyzyk)
               {
                    ile_krzyzykow++;
               }
           }
           if ((ile_krzyzykow==0)&&(ile_kolek==2))
           {
               tab[wiersz_pustych][kolumna_pustych]=krzyzyk;
               przegrana=true;
               return przegrana;
           }
       }
   }
   return przegrana;
}
void losuj_3x3(int **tab)
{
    int wiersz=-1,kolumna=-1;
    wiersz= (rand()%3);
    kolumna= (rand()%3);

    while (tab[wiersz][kolumna]!=puste)
    {
        wiersz= (rand()%3);
        kolumna= (rand()%3);
    }
    tab[wiersz][kolumna]=krzyzyk;
}

