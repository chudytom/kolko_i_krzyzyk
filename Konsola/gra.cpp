/**
  * \file gra.cpp
  * \brief Zawiera definicje funkcji sterujących grą
  */


#include <iostream>
#include <windows.h>
#include <conio.h>
#include "Naglowek.h"


using namespace std;

int postawiono_kolek=0, postawiono_krzyzykow=0;
Spole *tablica_kolek, *tablica_krzyzykow;

bool graj (int rodzaj_gry, int **tab, int w, int k, int ile_wygrywa, int numer_gry)
{
    tablica_kolek=tworz_tablice_pol(w,k);
    tablica_krzyzykow=tworz_tablice_pol(w,k);
    for (int i=0;i<(w*k/2+1);i++)
    {
        tablica_kolek[i].numer=0;
    }
    for (int i=0;i<(w*k/2+1);i++)
    {
        tablica_krzyzykow[i].numer=0;
    }
    //cout<< "Rodzaj gry to "<<rodzaj_gry<<endl;
    bool czy_gramy=true;
    drukuj(tab,w,k,rodzaj_gry);
    int ktory_ruch=0;
    bool warunek=true; // czy juz jest wygrana
    while (warunek)
    {
        warunek = wykonaj_ruch(rodzaj_gry,tab,w,k,ile_wygrywa,numer_gry,ktory_ruch);
    }
    for (int i=0;i<postawiono_kolek;i++)
    {
        tablica_kolek[i].numer=0;
    }
    for (int i=0;i<postawiono_krzyzykow;i++)
    {
        tablica_krzyzykow[i].numer=0;
    }
    postawiono_kolek=0;
    postawiono_krzyzykow=0;

    bool pytanie=true;
    while(pytanie)
    {
    cout<< "Czy chcesz zagrac jeszcze raz?"<<endl;
    cout<< "Napisz 'tak' jesli chcesz zagrac jeszcze raz."<<endl;
    cout<< "Napisz 'nie' jesli chcesz wrocic do menu"<<endl;
    string odpowiedz;
    cin>> odpowiedz;
        pytanie=false;
    if (odpowiedz=="tak")
    {
        czy_gramy=true;
        system("cls");
    }
    else if (odpowiedz=="nie")
    {
        czy_gramy=false;
    }
    else
    {
        pytanie=true;
        cout<< "Podaj dobra odpowiedz"<<endl;
    }
    }
    return czy_gramy;
}
bool wykonaj_ruch (int rodzaj_gry, int **tab, int w, int k, int ile_wygrywa, int numer_gry, int &ktory_ruch)
{
    ktory_ruch++;
    bool warunek=true; //czy juz jest wygrana
        if (ktory_ruch> (w * k))
        {
            cout<< "Koniec ruchow - remis"<<endl<<endl;
            return false;
        }
        if ((rodzaj_gry==1)||(rodzaj_gry==3)) // graja tylko przeciwnicy
        {
            ruch_gracza(tab,w,k,numer_gry,ktory_ruch,rodzaj_gry);
        }
        if (((rodzaj_gry==2)||(rodzaj_gry==4))&&(((ktory_ruch + numer_gry)%2)==0)) // ruchy gracza (vs. komputer)
        {
            ruch_gracza(tab,w,k,numer_gry,ktory_ruch,rodzaj_gry);
        }
        if (((rodzaj_gry==2))&&(((ktory_ruch + numer_gry)%2)==1)) // ruchy komputera (3x3)
        {

            cout<< "Ruch gracza "<<endl;
            cout<< "Podaj wspolrzedne pola, najpierw wiersz potem kolumna"<<endl;
            Sleep(500);
            ruch_komputera_3x3(tab,w,k,ktory_ruch, ile_wygrywa);
        }
        if (((rodzaj_gry==4))&&(((ktory_ruch + numer_gry)%2)==1)) // ruchy komputera (15x15)
        {
            cout<< "Ruch komputera 15x15"<<endl<<endl;
            Sleep(1000);
            ruch_komputera_15x15(tab,w,k,ktory_ruch,rodzaj_gry);
        }
        system("cls");
        drukuj(tab,w,k,rodzaj_gry);
        int wygrana=sprawdz_wygrana(tab,w,k,ile_wygrywa);
        if (wygrana==1) wygrane_kolka++;
        if (wygrana==2) wygrane_krzyzyka++;
        switch(wygrana)
        {
        case 0: warunek=true;
                break;
        case 1:
                warunek=false;
                cout<< "Gra zakonczona. Wygral gracz kolko"<<endl<<endl;
                break;
        case 2:
                warunek=false;
                cout<< "Gra zakonczona. Wygral gracz krzyzyk"<<endl<<endl;
                break;
        default:
                cout<<"Nie rozpoznaje takiego wyniku rozgrywki"<<endl;
                break;
        }
    return warunek;
}

int sprawdz_wygrana(int **tab, int w, int k, int ile_wygrywa)
{
   //wiersze
   for (int i=0;i<w;i++)
   {
       int ile_kolek=0, ile_krzyzykow=0, max_kolek=0, max_krzyzykow=0;
       for (int j=0;j<k;j++)
       {
           if (tab[i][j]==puste)
           {
               ile_kolek=0;
               ile_krzyzykow=0;
           }
           else if(tab[i][j]==kolko)
           {
               ile_kolek++;
               ile_krzyzykow=0;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
               ile_kolek=0;
           }
           if(ile_kolek>max_kolek)
           {
               max_kolek=ile_kolek;
           }
           if(ile_krzyzykow>max_krzyzykow)
           {
               max_krzyzykow=ile_krzyzykow;
           }
       }
       if(max_kolek==ile_wygrywa)
       {
           return 1;
       }
       if(max_krzyzykow==ile_wygrywa)
       {
           return 2;
       }
   }
   // kolumny
   for (int j=0;j<k;j++)
   {
       int ile_kolek=0, ile_krzyzykow=0, max_kolek=0, max_krzyzykow=0;
       for (int i=0;i<k;i++)
       {
           if (tab[i][j]==puste)
           {
               ile_kolek=0;
               ile_krzyzykow=0;
           }
           else if(tab[i][j]==kolko)
           {
               ile_kolek++;
               ile_krzyzykow=0;
           }
           else if(tab[i][j]==krzyzyk)
           {
               ile_krzyzykow++;
               ile_kolek=0;
           }
           if(ile_kolek>max_kolek)
           {
               max_kolek=ile_kolek;
           }
           if(ile_krzyzykow>max_krzyzykow)
           {
               max_krzyzykow=ile_krzyzykow;
           }
       }
       if(max_kolek==ile_wygrywa)
       {
           return 1;
       }
       if(max_krzyzykow==ile_wygrywa)
       {
           return 2;
       }
   }
   // zwykle przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=0;j<(k-ile_wygrywa+1);j++)
       {
           int ile_kolek=0, ile_krzyzykow=0, max_kolek=0, max_krzyzykow=0;
           for (int l=0;((l+i)<w)&&((l+j)<k);l++)
           {
               if (tab[i+l][j+l]==puste)
               {
                   ile_kolek=0;
                   ile_krzyzykow=0;
               }
               else if(tab[i+l][j+l]==kolko)
               {
                   ile_kolek++;
                   ile_krzyzykow=0;
               }
               else if(tab[i+l][j+l]==krzyzyk)
               {
                   ile_krzyzykow++;
                   ile_kolek=0;
               }
               if(ile_kolek>max_kolek)
               {
                   max_kolek=ile_kolek;
               }
               if(ile_krzyzykow>max_krzyzykow)
               {
                   max_krzyzykow=ile_krzyzykow;
               }
           }
           if(max_kolek==ile_wygrywa)
           {
               return 1;
           }
           if(max_krzyzykow==ile_wygrywa)
           {
               return 2;
           }
       }
   }
   // odwrotne przekatne
   for (int i=0;i<(w-ile_wygrywa+1);i++)
   {
       for (int j=(k-1);j>(ile_wygrywa-2);j--)
       {
           int ile_kolek=0, ile_krzyzykow=0, max_kolek=0, max_krzyzykow=0;
           for (int l=0;((l+i)<w)&&((j-l+1)>0);l++)
           {
               if (tab[i+l][j-l]==puste)
               {
                   ile_kolek=0;
                   ile_krzyzykow=0;
               }
               else if(tab[i+l][j-l]==kolko)
               {
                   ile_kolek++;
                   ile_krzyzykow=0;
               }
               else if(tab[i+l][j-l]==krzyzyk)
               {
                   ile_krzyzykow++;
                   ile_kolek=0;
               }
               if(ile_kolek>max_kolek)
               {
                   max_kolek=ile_kolek;
               }
               if(ile_krzyzykow>max_krzyzykow)
               {
                   max_krzyzykow=ile_krzyzykow;
               }
           }
           if(max_kolek==ile_wygrywa)
           {
               return 1;
           }
           if(max_krzyzykow==ile_wygrywa)
           {
               return 2;
           }
       }
   }
   return 0;
}
void ruch_gracza(int **tab, int w, int k, int numer_gry, int &ktory_ruch, int rodzaj_gry)
{
    int wiersz=0,kolumna=0;
    string gracz;
    if (((ktory_ruch+numer_gry)%2)==0)
    {
        gracz = "kolko";
    }
    else
    {
        gracz = "krzyzyk";
    }
    if (rodzaj_gry==1||rodzaj_gry==3 )
    {
    cout<< "Ruch gracza "<< gracz <<endl;
    cout<< "Podaj wspolrzedne pola, najpierw wiersz potem kolumna"<<endl;
    }
    if (rodzaj_gry==2||rodzaj_gry==4 )
    {
        if(gracz=="kolko")
        {cout<< "Ruch gracza"<<endl;}
        else if (gracz=="krzyzyk")
        {cout<< "Ruch komputera"<<endl;}
    cout<< "Podaj wspolrzedne pola, najpierw wiersz potem kolumna"<<endl;
    }
    bool warunek=true;
    while (warunek)
    {
        warunek=false;
        wiersz=pobierz_liczbe();
    while((wiersz<1)||(wiersz>w))
    {
        cout<< "Podaj liczbe jeszcze raz"<<endl;
        wiersz=pobierz_liczbe();
    }
    wiersz--;
    kolumna=pobierz_liczbe();
    while((kolumna<1)||(kolumna>k))
    {
        cout<< "Podaj liczbe jeszcze raz"<<endl;
        kolumna=pobierz_liczbe();
    }
    kolumna--;
    if (tab[wiersz][kolumna]!=puste)
    {
        cout<< "Wskazane pole jest juz zajete, wybierz inne"<<endl;
        warunek=true;
    }
    }
    if (((ktory_ruch + numer_gry)%2)==0)
    {
        tab[wiersz][kolumna]=kolko;
        stawiamy_kolko(tablica_kolek,wiersz,kolumna);
    }
    else
    {
        tab[wiersz][kolumna]=krzyzyk;
        stawiamy_krzyzyk(tablica_krzyzykow,wiersz,kolumna);
    }
}

