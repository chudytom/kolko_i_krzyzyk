/**
  * \file fun_podstawowe.cpp
  * \brief Plik zawierający funkcje wykorzystywane w różnych modułach, nieprzyporządkowane gdzie indziej
  */

#include <iostream>
#include <windows.h>
#include <conio.h>
#include <Naglowek.h>
#include <cmath>
#include <ctime>
#include <cstdlib>

using namespace std;


int **tworz_tablice(int w,int k)
{
    int **wsk = new int *[w];
    for (int i=0;i<w;i++){
        wsk[i]=new int[k];
    }
    return wsk;
}
void wypelnij_zerami(int **tab, int w, int k)
{
    for (int i=0;i<w;i++)
    {
        for (int j=0;j<k;j++)
        {
            tab[i][j]=puste;
        }
    }
}

void drukuj (int **tab, int w, int k, int rodzaj_gry)
{
    if (rodzaj_gry==2||rodzaj_gry==4)
    {
    cout<< "Komputer wygral "<< wygrane_krzyzyka<<" razy"<<endl;
    cout<< "Gracz wygral "<< wygrane_kolka<<" razy"<<endl;
    }
    if (rodzaj_gry==1||rodzaj_gry==3)
    {
    cout<< "Gracz krzyzyk wygral "<< wygrane_krzyzyka<<" razy"<<endl;
    cout<< "Gracz kolko wygral "<< wygrane_kolka<<" razy"<<endl;
    }
    cout<<endl;
    cout<< "   ";
    for (int i=0;i<k;i++)
    {
        cout<<i+1;
        if(i<9)
        {
        cout<<" ";
        }
    }
    cout<<endl;
    for (int i=0;i<w;i++)
    {
        cout<<i+1<<" ";
        if(i<9)
        {
        cout<<" ";
        }
        for (int j=0;j<k;j++)
        {
            if (tab[i][j]==puste)
            {
                cout<< " ";
            }
            else if (tab[i][j]==krzyzyk)
            {
                cout<< 'x';
            }
            else if (tab[i][j]==kolko)
            {
                cout<< 'o';
            }
            cout<< " ";
        }
        cout<<endl;
    }
    cout<<endl;
}
int pobierz_liczbe()
{
    bool warunek=true;
    string wiersz;
    while (warunek)
    {
        warunek=false;
        cin>>wiersz;
        for (int unsigned i=0;i<wiersz.size();i++)
        {
            if ((wiersz[i]<'0')||(wiersz[i]>'9'))
            {
                warunek=true;
                cout<< "Podaj liczbe"<<endl;
                break;
            }
        }

    }
    int pom=0;
    for(int unsigned i=0;i<wiersz.size();i++)
    {
        pom += (wiersz[i]-'0')*(pow(10,wiersz.size()-i-1));

    }
    int liczba=pom;
    return liczba;
}
Spole *tworz_tablice_pol(int w, int k)
{
    Spole *tab=new Spole[w*k/2+1];
    return tab;
}
void stawiamy_kolko(Spole *tab, int wiersz, int kolumna)
{
    tab[postawiono_kolek].wiersz=wiersz;
    tab[postawiono_kolek].kolumna=kolumna;
    postawiono_kolek++;
    tab[postawiono_kolek-1].numer=postawiono_kolek;
}
void stawiamy_krzyzyk(Spole *tab, int wiersz, int kolumna)
{
    tab[postawiono_krzyzykow].wiersz=wiersz;
    tab[postawiono_krzyzykow].kolumna=kolumna;
    postawiono_krzyzykow++;
    tab[postawiono_krzyzykow-1].numer=postawiono_krzyzykow;
}

